function reduce(elements, cb, startingValue) {
    let accumulator = (startingValue !== undefined) ? startingValue : elements[0];
    
    for(let index=1; index<elements.length; index++) {

        accumulator = cb(accumulator, elements[index]);
    }

    return accumulator;
}

module.exports = reduce;