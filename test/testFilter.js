const items = require('./data');
const filter = require('../filter');

const result = filter(items, number => {
    return number > 3;
});

console.log(result);