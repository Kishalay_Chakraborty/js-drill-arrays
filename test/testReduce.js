const items = require('./data');
const reduce = require('../reduce');

const sum = reduce(items, (acc, item) => {

    return acc + item;
});

console.log(sum);