const items = require('./data.js');
const each = require('../each.js');

each(items, (item, index) => {

    console.log(`Item : ${item}, Index : ${index}`);
});